package main

import (
	"github.com/gin-gonic/gin"
	"custom_deletePkg"
	"github.com/gin-gonic/gin/binding"
	sessions "github.com/tommy351/gin-sessions"
)
// Binding from form values
type form_all struct {
	Database_id 		string 	`form:"database_id" 	binding:"required"`
}

type form_single struct {
	Database_id 		string 	`form:"database_id" 	binding:"required"`
	Ebay_id				string 	`form:"ebay_id" 		binding:"required"`
	Auction_id			string 	`form:"auction_id" 		binding:"required"`
}

type form_file struct {
	File_name 			string 	`form:"file_name" 		binding:"required"`
}

type login_form struct {
	Login 	string 	`form:"login" 	binding:"required"`
	Pass 	string 	`form:"pass" 	binding:"required"`
}

func main() {
	r := gin.Default() //gin.New()
	r.LoadHTMLTemplates("form_login.html")
	r.LoadHTMLTemplates("form_delete.html")

	//session declaration
	store := sessions.NewCookieStore([]byte("YQEGRVOQUERygvaeouvqtYOUGygrvorq6t43167casgc182837346eysuqcv"))
	r.Use(sessions.Middleware("sellerfox_session", store))

	//http://localhost:8085/form_login
	r.GET("/form_login", func(c *gin.Context){
		obj := gin.H{"title": "Main form"}
		//draw html form
		c.HTML(200, "form_login.html", obj)
	})

	//http://localhost:8085/form_delete
	r.POST("/form_delete", func(c *gin.Context) {
		var login login_form
		c.BindWith(&login, binding.Form)

		session := sessions.Get(c)
		session.Set("login", login.Login)
		session.Set("pass", login.Pass)
		session.Save()

		if custom_deletePkg.Check_login(session.Get("login").(string), session.Get("pass").(string)) == true {
			obj := gin.H{"title": "Main form"}
			//draw html form
			c.HTML(200, "form_delete.html", obj)
		} else {
			c.Redirect(301, "/form_login")
		}
	})

	//http://localhost:8085/form_delete
	r.GET("/form_delete", func(c *gin.Context){
		session := sessions.Get(c)

		if 	session.Get("login") != nil && session.Get("pass") != nil {
			if custom_deletePkg.Check_login(session.Get("login").(string), session.Get("pass").(string)) == true {
				obj := gin.H{"title": "Main form"}
				//draw html form
				c.HTML(200, "form_delete.html", obj)
			}
		} else {
			c.Redirect(301, "/form_login")
		}
	})

	//http://localhost:8085/form_delete_all
	r.POST("/form_delete_all", func(c *gin.Context) {
		var form form_all
		c.BindWith(&form, binding.Form)

		//get a list of all gallery names that this user has
		galleries := custom_deletePkg.Get_galleries(form.Database_id)

		//get all ebay accounts linked to this sellerfox user
		ebay_users := custom_deletePkg.Get_ebay_users(form.Database_id)

		//delete all galleries from this user in all his ebay auctions
		custom_deletePkg.Custom_delete(ebay_users, galleries, form.Database_id)

		//after script finished working redirect to homepage
		c.Redirect(301, "/form")
	})

	//http://localhost:8085/form_single
	r.POST("/form_delete_single", func(c *gin.Context) {
		var form form_single
		c.BindWith(&form, binding.Form)

		//get a list of all gallery names that this user has
		galleries := custom_deletePkg.Get_galleries(form.Database_id)

		//get info about a specific ebay account linked to sellerfox user
		ebay_user := custom_deletePkg.Get_ebay_user(form.Database_id, form.Ebay_id)

		//delete all galleries from this user in a specific ebay auction
		custom_deletePkg.Custom_delete_single(form.Auction_id, ebay_user[0], galleries, form.Database_id)

		//after script finished working redirect to homepage
		c.Redirect(301, "/form")
	})

	//http://localhost:8085/form_restore_single
	r.POST("/form_restore_single", func(c *gin.Context) {
		var form form_single
		c.BindWith(&form, binding.Form)

		//get a list of all files in logs folder
		file_names := custom_deletePkg.Get_list_of_logs()

		//get only one relevant filename
		found_file_name := custom_deletePkg.Sort_files_by_three(file_names, form.Auction_id, form.Database_id, form.Ebay_id)

		//big function that restores ebay item description from file
		custom_deletePkg.Restore_from_file(found_file_name)

		//after script finished working redirect to homepage
		c.Redirect(301, "/form")
	})

	//http://localhost:8085/form_restore_all
	r.POST("/form_restore_all", func(c *gin.Context) {
		var form form_all
		c.BindWith(&form, binding.Form)

		//get a list of all files in logs folder
		file_names := custom_deletePkg.Get_list_of_logs()

		//get only relevant filenames
		found_file_names := custom_deletePkg.Sort_files_by_one(file_names, form.Database_id)

		//for each backup file found -> restore description
		for _, file_name := range found_file_names {
			//big function that restores ebay item description from file
			custom_deletePkg.Restore_from_file(file_name)
		}

		//after script finished working redirect to homepage
		c.Redirect(301, "/form")
	})

	//http://localhost:8085/form_restore_file
	r.POST("/form_restore_file", func(c *gin.Context) {
		var form form_file
		c.BindWith(&form, binding.Form)

		//big function that restores ebay item description from file
		custom_deletePkg.Restore_from_file(form.File_name)

		//after script finished working redirect to homepage
		c.Redirect(301, "/form")
	})

	r.POST("/form_log_out", func(c *gin.Context){
		session := sessions.Get(c)
		session.Clear()
		session.Save()

		c.Redirect(301, "/form_login")
	})

	// Listen and server on 0.0.0.0:8085
	r.Run(":8085")
}
