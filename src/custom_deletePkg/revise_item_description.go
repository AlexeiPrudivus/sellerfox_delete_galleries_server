package custom_deletePkg

import (
	"fmt"
)

func Revise_item_description(auction_id, auction_description string, ebay_user Ebay_user)(){
	//generate ReviseItem request xml
	revise_item_xml := Generate_revise_item_xml(auction_id, auction_description, ebay_user.Ebay_token)

	//execute Ebay_api_call function
	revise_item_response, _ := Ebay_api_call("ReviseItem", revise_item_xml, ebay_user.Site_id)

	//if there was an error in executing request
	has_errors := Response_has_errors(revise_item_response, "ReviseItem")
	if has_errors == false {
		fmt.Println("Request to restore description for auction",auction_id,"executed with status:",revise_item_response["status"])
	}
}
