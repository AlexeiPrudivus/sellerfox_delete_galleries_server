package custom_deletePkg

import (
	"regexp"
	"fmt"
)

func Remove_custom_gallery_code(auction_description string, galleries []Gallery)(string){
	//make sure to restore escaped characters in auction_description
	//fmt.Println(auction_description) //delete later
	auction_description = Escape(auction_description)
	//fmt.Println(auction_description) //delete later

	for _, gallery := range galleries {
		reg, error := regexp.Compile("(?s)<!-- Begi[n]{1,2} of Sellerfox Gallery \""+gallery.Name+"\" -->.+?<!-- End of Sellerfox Gallery \""+gallery.Name+"\" -->")
		if error != nil {
			fmt.Println(error)
		}
		auction_description = reg.ReplaceAllString(auction_description, "")
	}
	//fmt.Println(auction_description) //delete later

	return auction_description
}
