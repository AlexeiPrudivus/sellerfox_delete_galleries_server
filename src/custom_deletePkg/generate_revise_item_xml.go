package custom_deletePkg

func Generate_revise_item_xml(auction_id, auction_description, token string)(string){
	revise_item_xml :=  "<?xml version='1.0' encoding='UTF-8'?>"+
						"<ReviseItemRequest xmlns='urn:ebay:apis:eBLBaseComponents'>"+
							"<Version>"+Compatibility_level+"</Version>"+
							"<ErrorLanguage>en_US</ErrorLanguage>"+
							"<WarningLevel>High</WarningLevel>"+
							"<Item>"+
								"<ItemID>"+auction_id+"</ItemID>"+
								"<Description><![CDATA["+auction_description+"]]></Description>"+
								"<DescriptionReviseMode>Replace</DescriptionReviseMode>"+
							"</Item>"+
							"<RequesterCredentials>"+
							"<eBayAuthToken>"+token+"</eBayAuthToken>"+
							"</RequesterCredentials>"+
						"</ReviseItemRequest>"

	return revise_item_xml
}
