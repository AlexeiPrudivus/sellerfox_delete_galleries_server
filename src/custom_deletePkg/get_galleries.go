package custom_deletePkg

import (
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
)

type Gallery struct {
	Name string
}

func Get_galleries(database_id string)([]Gallery) {
//============================================== connect to sellerfox database ==============================================//
	//sql.Open("mysql", "db_username:db_password@protocol(address:port_num)/database_name")
	db, err := sqlx.Connect("mysql", "admin:KuL4nZg3htN1chT@tcp(ec2-46-137-76-114.eu-west-1.compute.amazonaws.com:3306)/sellerfox_"+database_id+"?charset=utf8")
	if err != nil {
		fmt.Println("Failed to connect to sellerfox.", err)
	} else {
		//fmt.Println("Connected to db sellerfox.")
	}
//======================================== get a list of auctions and gallery names =========================================//
	tx := db.MustBegin()
	//execute query
	rows := []Gallery{}
	var e error
	e = tx.Select(&rows,"select name from sfg_gallery")
	tx.Commit()
	if e != nil {
		fmt.Println("Could not get a list of galleries.")
		fmt.Println(e)
	}

	db.Close()
	//fmt.Println(rows) //delete later
	return rows
}
