package custom_deletePkg

import (
	"strings"
)

func Escape(escaped_string string)(string) {
	escaped_string = strings.Replace(escaped_string, "&#34;", "\"", -1) //"
	escaped_string = strings.Replace(escaped_string, "&lt;", "<", -1) // <
	escaped_string = strings.Replace(escaped_string, "&gt;", ">", -1) // >
	escaped_string = strings.Replace(escaped_string, "&#39;", "'", -1) // '
	escaped_string = strings.Replace(escaped_string, "&amp;", "&", -1) // &

	return escaped_string
}
