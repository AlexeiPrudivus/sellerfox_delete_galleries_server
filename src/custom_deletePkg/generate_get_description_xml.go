package custom_deletePkg

//import "strconv"

func Generate_get_description_xml(auction_id, token string)(string){
	get_description_xml :=  "<?xml version='1.0' encoding='utf-8'?>"+
							"<GetItemRequest xmlns='urn:ebay:apis:eBLBaseComponents'>"+
								"<DetailLevel>ItemReturnDescription</DetailLevel>"+
								"<ErrorLanguage>en_US</ErrorLanguage>"+
								"<WarningLevel>High</WarningLevel>"+
								"<ItemID>"+auction_id+"</ItemID>"+
								"<RequesterCredentials>"+
									"<eBayAuthToken>"+token+"</eBayAuthToken>"+
								"</RequesterCredentials>"+
							"</GetItemRequest>"

	return get_description_xml
}
