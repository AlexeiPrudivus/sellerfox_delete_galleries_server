package custom_deletePkg

import (
	"fmt"
	"time"
)

func Custom_delete(ebay_users []Ebay_user, galleries []Gallery, sf_login_id string) {
	continue_variable := "true"
	page_number := 1
	items_per_page := 200
	//set up time constraints
	t := time.Now().Local()
	end_time_from := string(t.Format("2006-01-02T15:04:05.768Z"))
	end_time_to := string(t.AddDate(0, 0, 90).Format("2006-01-02T15:04:05.768Z"))
	fmt.Println("end_time_from", end_time_from)
	fmt.Println("end_time_to", end_time_to)
	fmt.Println("Script started working. Please wait.")
//======================================== everything is happening within many loops ========================================//
	for _, ebay_user := range ebay_users {
		for continue_variable == "true" {
//========================================== get an array of auction_ids of a user ==========================================//
			//generate Xml configured to get auction ids from a certain user
			get_item_ids_xml := Generate_xml_ids(end_time_from, end_time_to, items_per_page, page_number, ebay_user.Ebay_id, ebay_user.Ebay_token)

			//execute Ebay_api_call function
			ebay_auctions_response, auction_ids := Ebay_api_call("GetSellerList", get_item_ids_xml, ebay_user.Site_id)

			//if there was an error in executing request
			has_errors := Response_has_errors(ebay_auctions_response, "GetSellerList")
			if has_errors == false {
				fmt.Println("Auction ids:", auction_ids)
				fmt.Println("User", ebay_user.Ebay_id, "; Page", page_number, "will be processed. It is not the last page:", ebay_auctions_response["has_more_items"])
				fmt.Println("Request to get auction ids from user", ebay_user.Ebay_id, "executed with status:", ebay_auctions_response["status"], "\n")
			}
//======================================= loop through auction_ids on the current page ======================================//
			for _, auction_id := range auction_ids {
				//remove galleries from auctions one by one
				Custom_delete_single(auction_id, ebay_user, galleries, sf_login_id)
			}
//========================================= check if there is need for another loop =========================================//
			continue_variable = ebay_auctions_response["has_more_items"]
			page_number++
		}
		continue_variable = "true"
		page_number = 1
	}
}
