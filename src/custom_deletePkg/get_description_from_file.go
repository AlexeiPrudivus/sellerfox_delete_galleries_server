package custom_deletePkg

import (
	"fmt"
	"io/ioutil"
)

func Get_description_from_file(file_name string)(string){
	//open file and read it's content into a byte, then convert it to string
	description_byte, error := ioutil.ReadFile("logs/"+file_name)
	if error != nil {
		fmt.Println("Can't find file error:",error)
	} else {
		fmt.Println(file_name,"file opened.")
	}
	description_string := string(description_byte)

	//fmt.Println(description_string) //delete later
	return description_string
}
