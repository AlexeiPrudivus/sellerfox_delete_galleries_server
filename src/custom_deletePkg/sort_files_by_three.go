package custom_deletePkg

import (
	"regexp"
)

func Sort_files_by_three(file_names []string, auction_id, database_id, ebay_id string)(string){
	//for each file in list check if ti fits criterias
	//16050_e-star-living_2015-02-24-10-27-18_101_351261964425.html
	found_file_name := ""
	for _, file_name := range file_names {
		reg := regexp.MustCompile(database_id+"_"+ebay_id+"_.*_"+auction_id+".html")
		if reg.FindString(file_name) != "" {
			found_file_name = file_name
		}
	}

	return found_file_name
}
