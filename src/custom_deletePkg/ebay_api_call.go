package custom_deletePkg
import (
	"github.com/PuerkitoBio/goquery"
	"net/http"
	"strings"
	"fmt"
	"regexp"
)

func Ebay_api_call(verb, request_xml_body, site_id string)(map[string]string, []string){
//=========================================== Generate and execute XmlHttpRequest ===========================================//
	client := &http.Client{}

	//will be used to return result
	var ebay_api_call_return map[string]string
	ebay_api_call_return = make(map[string]string)

	//prudivusPkg.Server_url+strconv.Itoa(item_id) //other url. to recieve html of the item page
	request, err := http.NewRequest("POST", Server_url_prod, strings.NewReader(request_xml_body)) //Server_url_prod
	if err != nil {
		fmt.Println("HttpRequest failed %s", err) //delete later
		ebay_api_call_return["status"] = "0"
		ebay_api_call_return["error"] = "Error generating request"
		ebay_api_call_return["error"] = "-2"
		return ebay_api_call_return, []string{""}
	}

	//add headers to request
	request.Header.Add("X-EBAY-API-COMPATIBILITY-LEVEL", Compatibility_level)
	request.Header.Add("X-EBAY-API-DEV-NAME", Dev_id_prod)
	request.Header.Add("X-EBAY-API-APP-NAME", App_id_prod)
	request.Header.Add("X-EBAY-API-CERT-NAME", Cert_id_prod)
	request.Header.Add("X-EBAY-API-SITEID", site_id)
	request.Header.Add("X-EBAY-API-CALL-NAME", verb)

	//execute request and get response
	response, err := client.Do(request)
	if err != nil {
		fmt.Printf("%s", err) //delete later
		ebay_api_call_return["status"] = "0"
		ebay_api_call_return["error"] = "Error sending request"
		ebay_api_call_return["error_id"] = "-1"
		return ebay_api_call_return, []string{""}
	}
	defer response.Body.Close()

	//html code of GetItem result
	responseDoc, err := goquery.NewDocumentFromResponse(response)
	if err != nil {
		fmt.Println("%s",err) //delete later
		ebay_api_call_return["status"] = "0"
		ebay_api_call_return["error"] = "Error using goquery"
		ebay_api_call_return["error"] = "-3"
		return ebay_api_call_return, []string{""}
	}
//============================================== if XML file has errors/warnings ============================================//
	errors :=responseDoc.Find("Errors") //fmt.Println(errors.Html())
	errors_html, _ := errors.Html()
	if errors_html != "" {
		errors_html = strings.Replace(errors_html, "&#34;", "\"", -1) //"
		errors_html = strings.Replace(errors_html, "&lt;", "<", -1) // <
		errors_html = strings.Replace(errors_html, "&gt;", ">", -1) // >
		errors_html = strings.Replace(errors_html, "&#39;", "'", -1) // '
		errors_html = strings.Replace(errors_html, "&amp;", "&", -1) // &
		//fmt.Println(request_xml_body) //delete later
		//fmt.Println(errors_html) //delete later

		//ebay_api_call_return must have errors or warnings if they will occur
		//set defaults to resultArray
		ebay_api_call_return["status"] = "1"
		ebay_api_call_return["error"] = ""
		ebay_api_call_return["error_id"] = ""
		ebay_api_call_return["warning_status"] = "0"
		ebay_api_call_return["warning"] = ""
		ebay_api_call_return["warning_id"] = ""

		//foreach error in errors
		responseDoc.Find("Errors").Each(func(i int, error *goquery.Selection) {
			//fmt.Println("error number", i) //index //delete later
			if i == 0 {
				//variables used within foreach loop
				short_msg := "";
				long_msg := "";
				error_id := "";

				//delete later
				//error_text := error.Find("longmessage").Text()
				//fmt.Println(error_text)
				error_code := error.Find("SeverityCode").Text()
				//fmt.Println("severity code value:", error_code)
//================================================ if XML file has warnings =================================================//
				if error_code == "Warning" {

					res_warning := ""
					/*if ebay_api_call_return["warning_status"] == "1" {
					continue
					}*/
					ebay_api_call_return["warning_status"] = "0"

					error_id = error.Find("ErrorCode").Text()
					short_msg = error.Find("ShortMessage").Text()
					long_msg = error.Find("LongMessage").Text()

					//fmt.Println("error_id:", error_id) //delete later
					//fmt.Println("short_msg:", short_msg) //delete later
					//fmt.Println("long_msg:", long_msg) //delete later

					res_warning = error_id+":"+short_msg

					//if there is a long message (ie ErrorLevel=1), display it
					if len(long_msg) > 0 {
						res_warning = error_id+":"+long_msg
					}

					ebay_api_call_return["warning_id"] = error_id
					ebay_api_call_return["warning"] = res_warning
				} else
//================================================== if XML file has errors =================================================//
				if error_code == "Error" {
					res_warning := ""
					ebay_api_call_return["status"] = "0"

					error_id = error.Find("ErrorCode").Text()
					short_msg = error.Find("ShortMessage").Text()
					long_msg = error.Find("LongMessage").Text()

					//fmt.Println("error_id:", error_id) //delete later
					//fmt.Println("short_msg:", short_msg) //delete later
					//fmt.Println("long_msg:", long_msg) //delete later

					res_warning = error_id+":"+short_msg

					//if there is a long message (ie ErrorLevel=1), display it
					if len(long_msg) > 0 {
						res_warning = error_id+":"+long_msg
					}

					ebay_api_call_return["error_id"] = error_id
					ebay_api_call_return["error"] = res_warning

					//TEMP
					if error_id == "10019" {
						fmt.Println("ErrorParameters:",error.Find("ErrorParameters").Text())
					}
				}
			}
		})
		return ebay_api_call_return, []string{""}
	} else {
//================================================= if XML file has no errors ===============================================//
		//get results nodes
		//responses := responseDoc.Find(verb+"Response");
		//responses_html, _ := responses.Html()
//========================================================== GetItem ========================================================//
		if verb == "GetItem" {
			//get only description
			ebay_api_call_return["description"], _ = responseDoc.Find("Description").Html()
		} else
//======================================================= GetSellerList =====================================================//
		if verb == "GetSellerList" {
			has_more_items, _ := responseDoc.Find("HasMoreItems").Html()
			ebay_api_call_return["has_more_items"] = has_more_items

			item_array, _ := responseDoc.Find("itemarray").Html()
			//ebay_api_call_return["item_array"] = item_array //delete later

			//find all auction_ids here
			reg, err := regexp.Compile("<item><itemid>([^\"]*?)</itemid></item>")
			if err != nil {
				fmt.Println(err)
			}
			reg_result_array := reg.FindAllStringSubmatch(item_array, -1)
			//fmt.Printf("%#v\n", reg_result_array) //delete later

			auction_ids := make([]string, len(reg_result_array))
			u := 0
			for i, reg_1 := range reg_result_array {
				for j, reg_2 := range reg_1 {
					if j == 1 {
						auction_ids[i] = reg_2
						u++
					}
				}
			}
			//fmt.Println("auction_ids:",auction_ids) //delete later
			ebay_api_call_return["status"] = "1"
			return ebay_api_call_return, auction_ids
		}

		//fmt.Println(responses.Html())
		//fmt.Println(responses)
		//responses := responseDoc.Find(verb+"Response");
		ebay_api_call_return["status"] = "1"
		//ebay_api_call_return["items"] = responses_html
		return ebay_api_call_return, []string{""}
	}
	//return map[string]string{}, []string{""}
	//description :=responseDoc.Find("Description")
	//fmt.Println(description.Html())
}
