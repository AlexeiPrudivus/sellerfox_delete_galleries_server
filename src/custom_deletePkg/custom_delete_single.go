package custom_deletePkg

import (
	"fmt"
)

func Custom_delete_single(auction_id string, ebay_user Ebay_user, galleries []Gallery, sf_login_id string)(){
	//generate Xml configured to get item description
	get_description_xml := Generate_get_description_xml(auction_id, ebay_user.Ebay_token)

	//execute Ebay_api_call function
	ebay_description_response, _ := Ebay_api_call("GetItem", get_description_xml, ebay_user.Site_id)

	//if there was an error in executing request
	has_errors := Response_has_errors(ebay_description_response, "GetItem")
	if has_errors == false {
		fmt.Print("Request to get description from auction ", auction_id, " executed with status: ", ebay_description_response["status"], "; ")
		auction_description := ebay_description_response["description"]
		//fmt.Println("Description:",auction_description) //delete later
//============================================ remove galleries from description ============================================//
		//modify item description. delete existing galleries
		auction_description = Remove_custom_gallery_code(auction_description, galleries)

		//escape special chars
		ebay_description_response["description"] = Escape(ebay_description_response["description"])

		//compare new description to old
		if ebay_description_response["description"] != auction_description {
			//if galleries were found and removed
			fmt.Println("At least one SellerFox gallery was found in auction", auction_id)

			//copy original description into a text file
			Save_description_to_file(ebay_user, ebay_description_response["description"], auction_id, sf_login_id)

			//copy new description into a text file
			//Save_description_to_file(ebay_user, auction_description, auction_id+"_new")

			//generate ReviseItem request xml
			revise_item_xml := Generate_revise_item_xml(auction_id, auction_description, ebay_user.Ebay_token)

			//execute Ebay_api_call function
			revise_item_response, _ := Ebay_api_call("ReviseItem", revise_item_xml, ebay_user.Site_id)

			//if there was an error in executing request
			has_errors = Response_has_errors(revise_item_response, "ReviseItem")
			if has_errors == false {
				fmt.Println("Request to delete galleries from auction",auction_id,"executed with status:",revise_item_response["status"])
			}
		} else {
			//if no galleries were found
			fmt.Println("Auction", auction_id, "has no SellerFox galleries from this user.")
		}
	}
}
