package custom_deletePkg

import (
	"strings"
)

type file_info struct {
	Sf_login_id, Ebay_id, Date, Site_id, Item_id string
}

func Parse_file_name(file_name string)(file_info){
	//16050_e-star-living_2015_02_24_10_27_18_101_351261964425.html
	var file_info file_info

	file_info_array := strings.Split(file_name, "_")
	//fmt.Println("file_info_array:",file_info_array)

	file_info.Sf_login_id = file_info_array[0]
	file_info.Ebay_id = file_info_array[1]
	file_info.Date = file_info_array[2]
	file_info.Site_id = file_info_array[3]
	file_info.Item_id =  strings.Replace(file_info_array[4], ".html", "", -1)

	/*
	fmt.Println("file_info_array[0]:",file_info_array[0])
	fmt.Println("file_info_array[1]:",file_info_array[1])
	fmt.Println("file_info_array[2]:",file_info_array[2])
	fmt.Println("file_info_array[3]:",file_info_array[3])
	fmt.Println("file_info_array[4]:",file_info_array[4])
	*/
	/*
	fmt.Println(file_info)
	fmt.Println(file_info.Sf_login_id)
	fmt.Println(file_info.Ebay_id)
	fmt.Println(file_info.Date)
	fmt.Println(file_info.Site_id)
	fmt.Println(file_info.Item_id)
	*/

	return file_info
}
