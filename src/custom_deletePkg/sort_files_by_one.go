package custom_deletePkg

import (
	"regexp"
)

func Sort_files_by_one(file_names []string, database_id string)([]string){
	//for each file in list check if ti fits criterias
	//16050_e-star-living_2015-02-24-10-27-18_101_351261964425.html
	found_file_names := make([]string, len(file_names))

	i := 0
	for _, file_name := range file_names {
		reg := regexp.MustCompile(database_id+"_.*")

		if reg.FindString(file_name) != "" {
			found_file_names[i] = file_name
			i++
		}
	}

	return found_file_names
}
