package custom_deletePkg

import (
	"time"
	"fmt"
	"os"
)

func Save_description_to_file(ebay_user Ebay_user, description, auction_id string, sf_login_id string)() {
	t := time.Now().Local()

	file, err := os.Create("logs/"+sf_login_id+"_"+ebay_user.Ebay_id+"_"+string(t.Format("2006-01-02-15-04-05"))+"_"+ebay_user.Site_id+"_"+auction_id+".html")
	if err != nil {
		fmt.Println("Error creating file:",err)
	}
	defer file.Close()

	_, err = file.WriteString(description)
	if err != nil {
		fmt.Println("Error writing to file:",err)
	}
}
