package custom_deletePkg

import (
	"io/ioutil"
)

func Get_list_of_logs()([]string){
	//get a list of all files
	files, _ := ioutil.ReadDir("logs/")

	//create array
	file_names_array := make([]string,len(files))

	//fill array with filenames
	for i, f := range files {
		file_names_array[i] = f.Name()
	}

	return file_names_array
}
