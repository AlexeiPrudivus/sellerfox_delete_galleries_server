package custom_deletePkg

import (
	"fmt"
)

func Restore_from_file(file_name string)(){
	//break filename into useful parts
	file_info := Parse_file_name(file_name)

	//get user token
	ebay_user := Get_ebay_user(file_info.Sf_login_id, file_info.Ebay_id)

	//get current description
	current_description := Get_current_description(file_info.Item_id, ebay_user[0])

	//get description from file
	file_description := Get_description_from_file(file_name)

	//if descriptions are different -> ReviseItem
	if current_description != file_description {
		Revise_item_description(file_info.Item_id, file_description, ebay_user[0])
	} else {
		fmt.Println("Current description and description from file are identical.")
	}
}
