package custom_deletePkg

import (
	"fmt"
)

func Response_has_errors(ebay_response map[string]string, verb string)(bool) {
	if ebay_response["status"] == "0" {
		if ebay_response["error"] != "" {
			fmt.Println(verb,"response error_body:", ebay_response["error"], "\n")
		} else if ebay_response["warning"] != "" {
			fmt.Println(verb,"response warning_body:", ebay_response["warning"], "\n")
		}
		return true
	} else {
		return false
	}
}
