package custom_deletePkg

import (
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
)

type Ebay_user struct {
	Ebay_id string
	Ebay_token string
	Site_id string
}

type Ebay_site struct {
	Site_id string
}

func Get_ebay_users(database_id string)([]Ebay_user) {
//============================================== connect to sellerfox database ==============================================//
	//sql.Open("mysql", "db_username:db_password@protocol(address:port_num)/database_name")
	db, err := sqlx.Connect("mysql", "admin:KuL4nZg3htN1chT@tcp(ec2-46-137-76-114.eu-west-1.compute.amazonaws.com:3306)/sellerfox_"+database_id+"?charset=utf8")
	if err != nil {
		fmt.Println("Failed to connect to sellerfox.", err)
	} else {
		//fmt.Println("Connected to db sellerfox.")
	}
//======================================== get a list of auctions and gallery names =========================================//
	tx := db.MustBegin()
	//execute query
	rows_user := []Ebay_user{}
	var e error
	e = tx.Select(&rows_user,"select ebay_id, ebay_token from sfg_settings order by ebay_id asc")
	tx.Commit()
	if e != nil {
		fmt.Println("Could not get a list of ebay_users.")
		fmt.Println(e)
	}
	db.Close()
//============================================== connect to sellerfox database ==============================================//
	//sql.Open("mysql", "db_username:db_password@protocol(address:port_num)/database_name")
	db, err = sqlx.Connect("mysql", "admin:KuL4nZg3htN1chT@tcp(ec2-46-137-76-114.eu-west-1.compute.amazonaws.com:3306)/sellerfox?charset=utf8")
	if err != nil {
		fmt.Println("Failed to connect to sellerfox.", err)
	} else {
		//fmt.Println("Connected to db sellerfox.")
	}
//======================================== get a list of auctions and gallery names =========================================//
	tx = db.MustBegin()
	//execute query
	rows_site := []Ebay_site{}
	e = tx.Select(&rows_site,"select site_id from sfg_load_auctions where sf_login_id = "+database_id+" order by ebay_id asc")
	tx.Commit()
	if e != nil {
		fmt.Println("Could not get a list of ebay_users.")
		fmt.Println(e)
	}
	db.Close()
	//fmt.Println(rows_user) //delete later

	for i:=0;i<len(rows_user);i++ {
		rows_user[i].Site_id = rows_site[i].Site_id
	}

	return rows_user
}
