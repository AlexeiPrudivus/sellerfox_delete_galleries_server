package custom_deletePkg

import (
	"fmt"
)

func Get_current_description(auction_id string, ebay_user Ebay_user)(string){
	//generate Xml configured to get item description
	get_description_xml := Generate_get_description_xml(auction_id, ebay_user.Ebay_token)

	//execute Ebay_api_call function
	ebay_description_response, _ := Ebay_api_call("GetItem", get_description_xml, ebay_user.Site_id)

	//if there was an error in executing request
	has_errors := Response_has_errors(ebay_description_response, "GetItem")
	if has_errors == false {
		fmt.Print("Request to get description from auction ", auction_id, " executed with status: ", ebay_description_response["status"], "; ")

		//escape special chars
		ebay_description_response["description"] = Escape(ebay_description_response["description"])

		return ebay_description_response["description"]
	}

	//if there was an error getting current description
	return "error"
}
